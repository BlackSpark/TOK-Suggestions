<?php
/**
 * Created by PhpStorm.
 * User: jonak
 * Date: 10/8/2016
 * Time: 11:33 AM
 */


$sql_connection = new mysqli($servername, $username, $password, $db, $port);

if ($sql_connection->connect_error) {
    die("Connection failed: " . $sql_connection->connect_error);
}

$table = $sql_connection->escape_string($_POST['table']);
$name = $sql_connection->escape_string($_POST['name']);
$suggestion = $sql_connection->escape_string($_POST['suggestion']);


$query = "INSERT INTO {$table} (suggestor, suggestion)
                                  VALUES ('{$name}', '{$suggestion}')";

$query_result = $sql_connection->query($query);

$sql_connection->close();

?>