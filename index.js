/**
 * Created by jonak on 10/8/2016.
 */
$(function () {

    var submitFunction = function(e)
    {
        var name = $("#name").val();
        var suggestion = $("#suggestion").val();
        var tablename = $("#period").val();
        console.log(name + " | " + suggestion + " | " + tablename);
        if(name === "" || suggestion === "" || tablename === null)
        {
            Materialize.toast("Please fill in everything!", 2000, "red white-text");
            return false;
        }
        else
        {
            Materialize.toast("Submitting...", 1000, "green");
            var card = $('.card');
            card.empty();
            card.append("<div class='center center-align card-content'><span class='card-title'><i class='material-icons'>check_circle</i> Submitted!</span></div>");
        }
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "submit.php", true);
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        xhr.send("name=" + name + "&suggestion=" + suggestion + "&table=" + tablename);
        return false;
    };

    $("select").material_select();
    $("#submitButton").click(submitFunction);
    $("form").submit(submitFunction);
});