<!DOCTYPE html>
<html>
<head>
    <title>ToK Entries Page</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="results.css"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<!--Import jQuery before materialize.js-->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="results.js"></script>
<br>
<div class="container">
    <div class="row">
        <div class="input-field col push-s3 s6">
            <select id="period">
                <option disabled selected value="">Choose your Period</option>
                <option value="per5">Period 5</option>
                <option value="per6">Period 6</option>
            </select>
            <label for="period">Period</label>
        </div>
    </div>
    <div class="row">
        <?php
        /**
         * Created by PhpStorm.
         * User: jonak
         * Date: 10/7/2016
         * Time: 9:14 PM
         */


        $sql_connection = new mysqli($servername, $username, $password, $db, $port);

        if ($sql_connection->connect_error) {
            die("Connection failed: " . $sql_connection->connect_error);
        }

        //echo "Connected Successfully <br>";

        if ($_GET['done'] === "true") {
            $result = $sql_connection->query("UPDATE {$_GET['tableId']}
                                                SET used=1
                                                WHERE id={$_GET['id']}");
        }

        $tables = array("per5", "per6");

        foreach ($tables as $selectedTable) {
            echo "<div class=\"periodSuggestions\" id=\"{$selectedTable}\">";

            $list = $sql_connection->query("SELECT * FROM {$selectedTable}");

            $ids = array();
            $suggestors = array();
            $suggestions = array();
            $used = array();
            $numused = 0;
            $mainSuggestionTitle = "Random Suggestion:";

            if ($list->num_rows > 0) {
                // output data of each row
                while ($row = $list->fetch_assoc()) {
                    $ids[] = $row['id'];
                    $suggestors[] = $row['suggestor'];
                    $suggestions[] = $row['suggestion'];
                    $isUsed = $row['used'] === "1";
                    $used[] = $isUsed;
                    if ($isUsed) $numused++;
                }
            } else {
                $mainSuggestionTitle = "No Suggestions!";
            }

            $randSelection = $_GET['sel'];
            if ($randSelection === NULL) $randSelection = -1;
            if ($numused != count($used) && $randSelection == -1) {
                while ($randSelection < 0 || $used[$randSelection]) {
                    $randSelection = rand(0, count($ids) - 1);
                }
            }
            if($numused == count($used) && $mainSuggestionTitle != "No Suggestions!")
                $mainSuggestionTitle = "No Undiscussed Suggestions!";

            $buttonOnclick = "return checkSure()";
            $buttonHref = "?id={$ids[$randSelection]}&sel={$randSelection}&tableId={$selectedTable}&done=true#{$selectedTable}";
            $buttonText = "";
            if ($used[$randSelection]){
                $buttonOnclick = "";
                $buttonHref = "#";
                $buttonText = "[already marked as discussed]";
            }
            else $buttonText = "Mark as Discussed";
            if ($numused === count($used)) $buttonText = "No more undiscussed suggestions!";
            $selcard = /** @lang HTML */
            <<< SELECTED_CARD
                        <div class="col s12 m12">
                        <div class="card green darken-3">
                            <div class="card-content white-text">
                                <span class="card-title">{$mainSuggestionTitle}</span>
                                <p class="suggestion">
                                    {$suggestions[$randSelection]}
                                    
                                </p>
                                <p class="suggestor">
                                - {$suggestors[$randSelection]}
</p>
                            </div>
                            <div class="card-action">
                                <a onclick="{$buttonOnclick}" href="{$buttonHref}">{$buttonText}</a>
</div>
                        </div>
                    </div>
                    <hr>
SELECTED_CARD;
            $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
            $selcard = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $selcard);
            echo $selcard;


            for ($i = 0; $i < count($ids); $i++) {
                $color = "";
                if ($used[$i])
                    $color = "grey lighten-1";
                else
                    $color = "blue-grey darken-1";

                $buttonClass = "";
                $buttonHref = "?id={$ids[$i]}&sel={$i}&tableId={$selectedTable}&done=false#{$selectedTable}";
                if ($used[$i]){;
                    $buttonClass = "blue-grey-text";
                }
                else $buttonText = "Select this Question";

                $card = /** @lang HTML */
                <<< SUGGESTION_CARD
                    <div class="col s12 m6">
                    <div class="card {$color}">
                        <div class="card-content white-text">
                            <span class="card-title">Suggestion {$ids[$i]}</span>
                            <p class="suggestion">
                                {$suggestions[$i]}
                                
                            </p>
                            <p class="suggestor">
                            
                                - {$suggestors[$i]}
</p>
                        </div>
                        <div class="card-action">
                                <a class="{$buttonClass}" href="{$buttonHref}">{$buttonText}</a>
</div>
                    </div>
                </div>
SUGGESTION_CARD;
                $url = '@(http)?(s)?(://)?(([a-zA-Z])([-\w]+\.)+([^\s\.]+[^\s]*)+[^,.\s])@';
                $card = preg_replace($url, '<a href="http$2://$4" target="_blank" title="$0">$0</a>', $card);
                echo $card;
            }
            echo "</div>";
        }


        $sql_connection->close();
        ?>
    </div>
</div>
</body>
</html>
