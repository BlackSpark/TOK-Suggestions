/**
 * Created by jonak on 10/8/2016.
 */
$(function() {

    var lhash = location.hash;
    console.log(lhash);
    if(lhash == "#per5")
    {
        $('#per6').css('display', 'none');
        $('#per5').css('display', 'block');
    }
    else if(lhash == "#per6")
    {
        $('#per5').css('display', 'none');
        $('#per6').css('display', 'block');
    }

    $('select').material_select();
    $('select#period').change(function () {
        var optionSelected = $(this).find("option:selected");
        var valueSelected  = optionSelected.val();
        switch(valueSelected)
        {
            case "per5":
                $('#per6').css('display', 'none');
                $('#per5').css('display', 'block');
                break;
            case "per6":
                $('#per5').css('display', 'none');
                $('#per6').css('display', 'block');
                break;
        }
    });

});

function checkSure()
{
    return confirm("Are you sure? This question will never again be suggested!");
}