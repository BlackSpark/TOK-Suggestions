<!DOCTYPE html>
<html>
<head>
    <title>ToK Suggestion Page</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="index.css" media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>

<body>
<!--Import jQuery before materialize.js-->
<script src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="index.js"></script>
<br>
<div class="container">

    <div class="row">
        <form class="col s12">
            <div class="card grey lighten-4">
                <div class="card-content black-text">
                    <span class="card-title">Submit a Topic Suggestion</span>
                    <div class="row">
                        <div class="input-field col m6 s12">
                            <input placeholder="Name" id="name" type="text">
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field col m6 s12">
                            <select id="period">
                                <option disabled selected value="">Choose your Period</option>
                                <option value="per5">Period 5</option>
                                <option value="per6">Period 6</option>
                            </select>
                            <label for="period">Period</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col s12">
                            <textarea id="suggestion" class="materialize-textarea"></textarea>
                            <label for="suggestion">Your Suggestion</label>
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <div class="row">
                        <div class="input-field">
                            <button id="submitButton" class="btn waves-light waves-effect">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="center center-align">
        <a class="waves-effect waves-light grey lighten-1 black-text btn-large" href="results.php">View Entries</a>
    </div>
</div>

</body>
</html>